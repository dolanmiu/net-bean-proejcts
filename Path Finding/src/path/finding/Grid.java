/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package path.finding;
import java.util.ArrayList;

/**
 *
 * @author Dollface
 */
public class Grid {
    
    private ArrayList<Vertex> vertices = new ArrayList<Vertex>();
    private ArrayList<Edge> edges = new ArrayList<Edge>();
    private ArrayList<Vertex> s = new ArrayList<Vertex>();
    private int dimensions;
    
    public Grid(int dimensions, ArrayList<Vertex> vertices, ArrayList<Edge> edges) {
        this.vertices = vertices;
        this.edges = edges;
        this.dimensions = dimensions;
    }
    
    public void printGrid() {
        String[][] grid = new String[dimensions][dimensions];
        System.out.println("Grid printing...");
        Vertex current = s.get(s.size()-1);
        for (int i = 0; i<s.size(); i++) {
            grid[s.get(i).getX()][s.get(i).getY()] = Integer.toString(s.get(i).getValue());
        }
        grid[0][0] = "x";
        grid[dimensions-1][dimensions-1] = "x";
        
        while(current!=null) {
            grid[current.getX()][current.getY()] = "x";
            current = current.parent;
        }
        for (int i = 0; i<dimensions; i++) {
            for (int j = 0; j<dimensions; j++) {
                System.out.print(grid[i][j]);
            }
                System.out.println("");
        }

    } 
    
    public void dijkstra() {
        initialization();
        while (!vertices.isEmpty()) {
            Vertex u = VertexMin();
            s.add(u);
            vertices.remove(u);
            
            for(int i = 0; i<Edge.getConnectedV(u).size(); i++) {
                Vertex v = Edge.getConnectedV(u).get(i);
                relax(u,v);
            }
        }
    }
    
    private Vertex VertexMin() {
        int lowestNumb = Integer.MAX_VALUE;
        Vertex lowestVertex = null;
        for (int i = 0; i<vertices.size(); i++) {
            if (vertices.get(i).weightEst < lowestNumb) {
                lowestNumb = vertices.get(i).weightEst;
                lowestVertex = vertices.get(i);
            }
        }
            return lowestVertex;
    }
    
    private void initialization() {
        vertices.get(0).weightEst = 0;
        vertices.get(0).parent = null;
        for (int i = 1; i<vertices.size(); i++) {
            vertices.get(i).weightEst = Integer.MAX_VALUE;
            vertices.get(i).parent = null;
        }
    }
    
    private void relax(Vertex u, Vertex v) {
        if (v.weightEst > u.weightEst + Edge.getWeight(u,v)) {
            v.weightEst = u.weightEst + Edge.getWeight(u,v);
            v.parent = u;
        }
    }
    
}
