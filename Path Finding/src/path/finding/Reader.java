/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package path.finding;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Dollface
 */
public class Reader {
    static int dimensions;
    int linecount = -1;
    int id = -1;
    ArrayList<Character> vertexNames = new ArrayList<Character>();
    static ArrayList<Edge> allEdges = new ArrayList<Edge>();
    static ArrayList<Vertex> allVertices = new ArrayList<Vertex>();
    
    public Reader(String filename) {
        fFile = new File(filename);
    }
    
    public final void doEverything() throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(fFile));
        dimensions = Integer.parseInt(scanner.nextLine());
        try {
            while(scanner.hasNextLine()) {
                processLine(scanner.nextLine());
            }
        }
        finally {
            scanner.close();
        }
        printStringGrid();
        transformEdges();
        Grid graph = new Grid(dimensions, allVertices, allEdges);
        graph.dijkstra();
        graph.printGrid();
    }
    
    protected void processLine(String aLine) {
        linecount += 1;
        Scanner scanner = new Scanner(aLine);
        scanner.useDelimiter("");
        String line = scanner.nextLine();
        for (int i = 0; i<dimensions; i++) {
            char name = line.charAt(i);
            vertexNames.add(name);
            createVertexGrid(linecount, line, i);
        }

    }
    
    private void testVariables() {
        System.out.println("Grid Size: " + dimensions + " x " + dimensions);
        for (int j = 0; j<vertexNames.size(); j++) {
            System.out.println(vertexNames.get(j));
            
        }
    }
    
    private void createVertexGrid(int count, String line, int i) {
        id += 1;
        int value = Character.getNumericValue(line.charAt(i));
        allVertices.add(new Vertex(id, value, count, i));
    }
    
    private void transformEdges() {
        int count = -1;
        for (int i = 0; i<(dimensions); i++) {
            for (int j = 0; j<(dimensions); j++) {
                if ((getVertex(i,j).getId() + 1) %dimensions != 0) {
                    count += 1;
                    allEdges.add(new Edge(count, getVertex(i,j), getVertex(i,j+1), getVertex(i,j).getValue() + getVertex(i,j+1).getValue()));
                }
                
            }
        }
        for (int i = 0; i<(dimensions); i++) {
            for (int j =0; j<(dimensions); j++) {
                if ((getVertex(i,j).getId() + 1) <= (dimensions*dimensions - dimensions)) {
                    count += 1;
                    allEdges.add(new Edge(count, getVertex(i,j), getVertex(i+1,j), getVertex(i,j).getValue() + getVertex(i+1,j).getValue()));
                }
                
            }
        }
    }
    
    private static Vertex getVertex(int x, int y) {
        for(int i=0; i<allVertices.size(); i++) {
            Vertex v = allVertices.get(i);
            if(v.getX() == x && v.getY() == y) return v;
	}
        return null;
    }
    
    public static void printStringGrid() {
        System.out.println("Original Grid:");
        for (int i = 0; i<dimensions; i++) {
            for (int j = 0; j<dimensions; j++) {
                System.out.print(getVertex(i,j).getValue());
            }
            System.out.println("");
        }
    }
    
    private final File fFile;
}
