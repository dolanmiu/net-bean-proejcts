/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package path.finding;
import java.util.ArrayList;

/**
 *
 * @author Dollface
 */
public class Edge {
    
    public final Vertex source, destination;
    public final int weight;
    public final int id;
    private static ArrayList<Edge> allEdges = new ArrayList<Edge>();
    
    public Edge(int id, Vertex source, Vertex destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
        this.id = id;
        addEdge();
    }
    
    public Vertex getSource() {
        return source;
    }
    
    public Vertex getDestination() {
        return destination;
    }
    
    public int id() {
        return id;
    }
    
    private void addEdge() {
        allEdges.add(this);
    }
    
    public static ArrayList<Edge> getConnected(Vertex v) {
        ArrayList<Edge> tempEdge = new ArrayList<Edge>();
        for (int i = 0; i<allEdges.size(); i++) {
            if (allEdges.get(i).getSource() == v || allEdges.get(i).getDestination() == v) {
                tempEdge.add(allEdges.get(i));
            } 
        }
        return tempEdge;
    }
    
    public static ArrayList<Vertex> getConnectedV(Vertex v) {
        ArrayList<Vertex> tempVertex = new ArrayList<Vertex>();
        for (int i = 0; i<allEdges.size(); i++) {
            if (allEdges.get(i).getSource() == v) {
                tempVertex.add(allEdges.get(i).getDestination());  
                    }
            if (allEdges.get(i).getDestination() == v) {
                tempVertex.add(allEdges.get(i).getSource());
            }
        }
        return tempVertex;
    }
    
    public int getWeight() {
        return this.weight;
    }
    
    public boolean contains(Vertex v) {
        if (source == v || destination == v) {
            return true;
        }
        return false;
    }
    
    public static int getWeight(Vertex u, Vertex v) {
        for (int i = 0; i<allEdges.size(); i++) {
            for (int j = 0; j<allEdges.size(); j++) {
                if (allEdges.get(i).getSource() == u && allEdges.get(j).getDestination() == v) {
                    return allEdges.get(i).weight;
                }
            }
        }
        return -1;
    }
    
    public static void print() {
        for (int i = 0; i<allEdges.size(); i++) {
            System.out.println(allEdges.get(i).toString());
        }
    }
    
    @Override
    public String toString() {
        return this.source + " " + this.destination;
    }
}
