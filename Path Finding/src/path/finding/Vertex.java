/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package path.finding;
import java.util.ArrayList;

/**
 *
 * @author Dollface
 */
public class Vertex {
    private final int id;
    private final int value, x, y;
    private static ArrayList<Vertex> allVertices = new ArrayList<Vertex>();
    public Vertex parent;
    public int weightEst;
    
    public Vertex(int id, int value, int x, int y) {
        this.id = id;
        this.value = value;
        this.x = x;
        this.y = y;
        addVertex();
    }
    
    public int getId() {
        return id;
    }
    
    public int getValue() {
        return value;
    }
    
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public ArrayList<Vertex> getAllVertices() {
        return allVertices;
    }
    
    private void addVertex() {
        allVertices.add(this);
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Integer.toString(this.id) == null) ? 0 : Integer.toString(this.id).hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return Integer.toString(this.id);
    }
}
