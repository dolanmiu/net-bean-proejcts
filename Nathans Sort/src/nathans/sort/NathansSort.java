/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nathans.sort;

/**
 *
 * @author Dollface
 */
public class NathansSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] studentScores = {1,2,3,4,5,6,7,8,9,10};
        int[] sortedStudents = new int[studentScores.length];
        //int[] seatingPlan;
        
        //bubble sort algorithm - standard algorithm used to sort it from lowest to highest
        for (int i = 0; i < studentScores.length; i++) {
            for (int j = 0; j < studentScores.length-1; j++) {
		if (studentScores[j] > studentScores[j+1]) {
                    int temp = studentScores[j+1];
                    studentScores[j+1] = studentScores[j];
                    studentScores[j] = temp;
        	
                }
            }
        }
        System.out.println(studentScores);
    }
}
