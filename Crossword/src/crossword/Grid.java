/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package crossword;
/**
 *
 * @author Dollface
 */
public class Grid {
    char[][] grid;
    int height, width;
    
    public Grid(int h, int w) {
        height = h; width = w;
        grid = new char[height][width];
        for(int i = 0; i<height; i++) {
            for(int j = 0; j<width; j++) {
                grid[i][j] = '.';
            }
        } 
    }
    
    public void addword(String x, String y, String o, String word) {
        int xcoord = Integer.parseInt(x);
        int ycoord = Integer.parseInt(y);
        char orientation = o.charAt(0);
        char[] splitword = word.toCharArray();
        if (orientation == 'v') {
            for(int i = 0; i<splitword.length; i++) {
                grid[xcoord][i+ycoord] = splitword[i];
            } 
        } else {
            for(int i = 0; i<splitword.length; i++) {
                grid[i+xcoord][ycoord] = splitword[i];
            }
        }
    }
        
    public void printxword() {
        for(int i = 0; i<width; i++) {            
            for(int j = 0; j<height; j++) {
                System.out.print(grid[j][i]);
            }
            System.out.println("");
        }
    }
        
    
}
