/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle.problem;
import java.lang.Math;
import java.util.ArrayList;

/**
 *
 * @author Dollface
 */
public class Triangle {
    private final int perimeter;
    int numberofTriangles = 0;
    private ArrayList<ArrayList<Number>> triangles = new ArrayList<ArrayList<Number>>();
    
    public Triangle(int p) {
        this.perimeter = p;
        generateTriangles(p);
    }
    
    private void generateTriangles(int p) {
        for (int i = 1; i<p; i++) {
            for (int j = 1; j<p; j++) {
                if (Math.pow(i,2) + Math.pow(j,2) == Math.pow(p-i-j,2)) {
                    ArrayList<Number> currTriangle = new ArrayList<Number>();
                    numberofTriangles += 1;
                    currTriangle.add(i);
                    currTriangle.add(j);
                    currTriangle.add(p-i-j);
                    triangles.add(currTriangle);
                }
            }
        }
        //dimensions = new int[numberofTriangles][3];
    }
    
    public int returnTriangles() {
        return numberofTriangles;
    }
    
    public int returnPerimeter() {
        return perimeter;
    }
    
    public ArrayList<ArrayList<Number>> returnDimensions() {
        return triangles;
    }
}
