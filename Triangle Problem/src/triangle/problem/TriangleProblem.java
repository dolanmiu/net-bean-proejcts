/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle.problem;
import java.util.ArrayList;

/**
 *
 * @author Dollface
 */
public class TriangleProblem {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Triangle> triangles = new ArrayList<Triangle>();
        System.out.println("Here is the amount of solutions:");
        for (int i = 0; i<500; i++) {
            triangles.add(new Triangle(i));
            //System.out.println(triangles.get(i).returnTriangles());
        }
        System.out.println("Number of Edges: " + getLargest(triangles).returnTriangles());
        System.out.println(getLargest(triangles).returnDimensions());
        System.out.println("Perimeter: " + getLargest(triangles).returnPerimeter());
        
    }
    
    public static Triangle getLargest(ArrayList<Triangle> triangles) {
        Triangle largest = triangles.get(0);
        for (int i = 0; i<triangles.size(); i++) {
            if (triangles.get(i).returnTriangles() > largest.returnTriangles()) {
                largest = triangles.get(i);
            }
        }
        return largest;
    }
}
