/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package levenshtein.distance;
import java.util.Scanner;

/**
 *
 * @author Dollface
 */
public class LevenshteinDistance {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String word1, word2;
        int distance;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a word");
        word1 = scanner.nextLine();
        System.out.println("Please enter a second word");
        word2 = scanner.nextLine();
        
        Levenshtein algorithm = new Levenshtein();
        
        distance = algorithm.find(word1, word2);
        System.out.println("The distance from the two words is " + distance);
        
        
    }
}
