/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package levenshtein.distance;
import java.lang.Math;

/**
 *
 * @author Dollface
 */
public class Levenshtein {
    String word1, word2;
    int matrixX, matrixY;
    int[][] matrix;
    int answer;
    
    public int find(String w1, String w2) {
        word1 = w1;
        word2 = w2;
        matrixX = word1.length()+1;
        matrixY = word2.length()+1;
        matrix = new int[matrixX][matrixY];
        //System.out.println(matrixX);
        //System.out.println(matrixY);
        for(int i = 0; i<matrixX; i++) {
            matrix[i][0] = i;
        }
        for(int j = 0; j<matrixY; j++) {
            matrix[0][j] = j;
        }
        //Matrix set up complete
        //printmatrix();
        
        for(int i = 0; i<word1.length(); i++) {
            for(int j = 0; j<word2.length(); j++) {
               if (wordcheck(word1.charAt(i),word2.charAt(j))) {
                    matrix[i+1][j+1] = matrix[i][j];
                } else {
                   matrix[i+1][j+1] = minimum(matrix[i][j+1]+1,matrix[i+1][j]+1,matrix[i][j]+1);
                }
            }
            
        }
        printmatrix();
        answer = matrix[matrixY-1][matrixX-1];
        return answer;
    }
    
    public boolean wordcheck(char a, char b) {
        if (a == b) {
            return true;
        }
        return false;
    }  
    
    public void printmatrix() {
        System.out.println("The current matrix looks like:");
        for(int j = 0; j<matrixX; j++) {
            for(int i = 0; i<matrixY; i++) {
                System.out.print(matrix[i][j]);
            }
            System.out.print("\n");
        }
    }
    public int minimum(int a, int b, int c) {
        int min;
        min = Math.min(Math.min(a, b),c);
        return min;
    }
}


