/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractions;

/**
 *
 * @author Dollface
 */
public class Fraction {
    public final int numerator, denominator;
    
    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }
    
    public void print(String c) {
        if (c.equals("raw")) {
            System.out.println(numerator + "/" + denominator);
        }
        
        if (c.equals("mixed")) {
            if (numerator > denominator) {
                int whole = 0;
                int newNumerator = numerator;
                while (newNumerator > denominator) {
                    newNumerator -= denominator;
                    whole += 1;
                }
                if (newNumerator == denominator) {
                    System.out.println(whole*newNumerator);
                } else {
                    System.out.println(whole + " " + newNumerator + "/" + denominator);
                }
            } else {
                System.out.println("Numerator is smaller than Denominator. Cannot be done!");
            }
        }
        
        if (c.equals("top heavy")) {
            Fraction newfraction = Operations.simplify(this);
            if (newfraction.denominator == 1) {
                System.out.println(newfraction.numerator);
            } else {
                System.out.println(newfraction.numerator + "/" + newfraction.denominator);
            }
        }
    }
    
    public void multiply(Fraction one, Fraction two) {

    }
    
}
