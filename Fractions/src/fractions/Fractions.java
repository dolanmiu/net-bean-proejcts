/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractions;
import java.util.Scanner;

/**
 *
 * @author Dollface
 */
public class Fractions {
        private static Fraction a, b;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
   
        System.out.println("Hi, what would you like to do?");
        System.out.println(" 1. Add \n 2. Subtract \n 3. Multiply \n 4. Find the Reciprocal \n 5. Convert Fraction to Decimal \n 6. Convert Decimal to Fraction");
        switch (Integer.parseInt(scanner.nextLine())) {
            case 1: 
                System.out.println("Enter a fraction with numerator and demoniator after a space, then press Enter, then repeat.");
                process(scanner.nextLine(),scanner.nextLine());
                Operations.Add(a,b).print("raw");
                break;
            case 2:
                System.out.println("Enter a fraction with numerator and demoniator after a space, then press Enter, then repeat.");
                process(scanner.nextLine(),scanner.nextLine());
                Operations.Subtract(a,b).print("raw");
                break;
            case 3:
                System.out.println("Enter a fraction with numerator and demoniator after a space, then press Enter, then repeat.");
                process(scanner.nextLine(),scanner.nextLine());
                Operations.Multiply(a,b).print("raw");
                break;
            case 4:
                System.out.println("Enter a fraction with numerator and demoniator after a space, then press Enter");
                process(scanner.nextLine(),null);
                Operations.Reciprocal(a).print("raw");
                break;
            case 5:
                System.out.println("Enter a fraction with numerator and demoniator after a space, then press Enter");
                process(scanner.nextLine(),null);
                System.out.println(Operations.convertToDecimal(a));
                break;
            case 6:
                System.out.println("Enter a decimal, then press Enter");
                Operations.convertToFraction(scanner.nextLine()).print("raw");
                break;
                
        }
        
    }
    private static void process(String inputA, String inputB) {
        String s[] = inputA.split(" ");
        int numeratorA = Integer.parseInt(s[0]);
        int denominatorA = Integer.parseInt(s[1]);
        a = new Fraction(numeratorA, denominatorA);
        
        if (inputB != null) {
            String p[] = inputB.split(" ");
            int numeratorB = Integer.parseInt(p[0]);
            int denominatorB = Integer.parseInt(p[1]);
            b = new Fraction(numeratorB, denominatorB);
        }
        
    }
            
}
