/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractions;
import java.math.BigDecimal;
import java.lang.Math;

/**
 *
 * @author Dollface
 */
public class Operations {
    
    public static Fraction Multiply(Fraction a, Fraction b) {
        int numerator = a.numerator * b.numerator;
        int denominator = a.denominator * b.denominator;
        Fraction newfraction = new Fraction(numerator,denominator);
        return simplify(newfraction);
    }
    
    public static Fraction simplify(Fraction a) {
        int newNumerator = a.numerator, newDenominator = a.denominator;
        for (int i = 2; i<10;i++) {
            while (newNumerator%i == 0 && newDenominator%i == 0) { //is divisible
                newNumerator = newNumerator/i;
                newDenominator = newDenominator/i;
            }
        }
        Fraction newfraction = new Fraction(newNumerator, newDenominator);
        return newfraction;
    }
    
    public static Fraction Reciprocal(Fraction a) {
        int newNumerator = a.denominator;
        int newDenominator = a.numerator;
        Fraction newfraction = new Fraction(newNumerator, newDenominator);
        return newfraction;
    }
    
    public static Fraction Add(Fraction a, Fraction b) {
        int newDenominator = a.denominator * b.denominator;
        int numeratorA = a.numerator * b.denominator;
        int numeratorB = b.numerator * a.denominator;
        int newNumerator = numeratorA + numeratorB;
        Fraction newfraction = new Fraction(newNumerator, newDenominator);
        return simplify(newfraction);
    }
    
    public static Fraction Subtract(Fraction a, Fraction b) {
        int newDenominator = a.denominator * b.denominator;
        int numeratorA = a.numerator * b.denominator;
        int numeratorB = b.numerator * a.denominator;
        int newNumerator = numeratorA - numeratorB;
        Fraction newfraction = new Fraction(newNumerator, newDenominator);
        return simplify(newfraction);
    }
    
    public static float convertToDecimal(Fraction a) {
        return (float)a.numerator/(float)a.denominator;
    }
    
    public static Fraction convertToFraction(String a) {
        BigDecimal decimal = new BigDecimal(a);
        int length = getNumberOfDecimalPlaces(decimal);
        int factor = (int)Math.pow(10, length);
        int numerator = (int)(factor * Double.valueOf(a));
        int denominator = factor;
        return simplify(new Fraction(numerator, denominator));
    }
    
    private static int getNumberOfDecimalPlaces(BigDecimal bigDecimal) {
        String string = bigDecimal.stripTrailingZeros().toPlainString();
        int index = string.indexOf(".");
        return index < 0 ? 0 : string.length() - index - 1;
        //return bigDecimal.scale();
    }
}
