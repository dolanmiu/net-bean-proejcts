/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix.transformations;
import java.lang.Math;

/**
 *
 * @author Dollface
 */
public class matrixtask {
    double[] coord = new double[2];
    String[] command;
    
    public matrixtask() {

    }
    
    public double[] compute(String c, String x, String y) {
        coord[0] = Integer.parseInt(x);
        coord[1] = Integer.parseInt(y);
        
        command = c.split(" ");

        if ("transform".equals(command[0])) {
            transform();
        }
        if ("translate".equals(command[0])) {
            translate();
        }
        if ("enlarge".equals(command[0])) {
            enlarge();
        }
        if ("rotate".equals(command[0])) {
            rotate();
        }
        if ("reflect".equals(command[0])) {
            reflect();
        }
        
        coord[0] = round(coord[0],1);
        coord[1] = round(coord[1],1);
        
        return coord;
    }
    
    public int[] transform() {
        
        return null;
    }
    
    public void translate() {
        int x = Integer.parseInt(command[1]);
        int y = Integer.parseInt(command[2]);
        coord[0] += x;
        coord[1] += y;
        
    }
    
    public void enlarge() {
        int scalefactor = Integer.parseInt(command[1]);
        coord[0] *= scalefactor;
        coord[1] *= scalefactor;
    }
    
    public void rotate() {
        double x, y;
        if (command[1].equals("deg")) {
            double theta;
            if (command[3].equals("anticlockwise")) {
                theta = Double.parseDouble(command[2]);
            } else {
                theta = Double.parseDouble(command[2])*-1;
            }
                double degrees = theta*(180/Math.PI);
                x = Math.cos(degrees)*coord[0] - Math.sin(degrees)*coord[1];
                y = Math.sin(degrees)*coord[0] + Math.cos(degrees)*coord[1];
                coord[0] = x;
                coord[1] = y;
            } else {
            double theta;
            if (command[2].equals("anticlockwise")) {
                theta = Double.parseDouble(command[1]);
            } else {
                theta = Double.parseDouble(command[1])*-1;
            }
                x = Math.cos(theta)*coord[0] - Math.sin(theta)*coord[1];
                y = Math.sin(theta)*coord[0] + Math.cos(theta)*coord[1];
                coord[0] = x;
                coord[1] = y;
            }
    }
    
    public void reflect() {
        
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value*factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
}
