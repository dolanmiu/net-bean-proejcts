/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix.transformations;
import java.util.Scanner;
/**
 *
 * @author Dollface
 */
public class MatrixTransformations {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String input;
        String[] coordinates;
        double[] output;
        String command;
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hi, please input two coordinates x and y seperated by a space.");
        input = scanner.nextLine();
        coordinates = input.split(" ");
        System.out.println("Good. Now input the command followed by the arguments:");
        command = scanner.nextLine();
        
        matrixtask matrix = new matrixtask();
        output = matrix.compute(command, coordinates[0],coordinates[1]);
        System.out.println(output[0] + " " + output[1]);
        
    }
}
