package tube6;

import java.util.Scanner;

public class Tube6 {
    public static void main(String[] args) {
        //LIMITATION: typing stations into program is case sensitive! Can be left as is, or changed by
        //using equalsIgnoreCase(...) instead of (equals...) check things over to make sure theres no dumb errors.
        Line bakerloo = new Line("Bakerloo", new String[]{"Paddington", "Edgware Road", "Baker Street", "Regent's Park", "Oxford Circus", "Piccadilly Circus", "Charing Cross", "Embankment", "Waterloo", "Lamberth North", "Elephant & Castle"});
        Line circle = new Line("Circle", new String[]{"Edgware Road", "Baker Street", "Great Portland Street", "Euston Square", "King's Cross St. Pancras", "Farringdon", "Barbican", "Moorgate", "Liverpool Street", "Aldgate", "Tower Hill", "Bank/Monument","Cannon Street", "Mansion House", "Blackfriars", "Temple", "Embankment", "Westminster", "St. James's Park", "Victoria", "Sloane Square", "South Kensington", "Gloucester Road", "High Street Kensington", "Notting Hill Gate", "Bayswater", "Paddington", "Edgware Road"});
        Line jubilee = new Line("Jubilee", new String[]{"Baker Street","Bond Street","Green Park", "Westminster", "Waterloo", "Southwark", "London Bridge"});
        Line central = new Line("Central", new String[]{"Notting Hill Gate","Queensway", "Lancaster Gate", "Marble Arch", "Bond Street", "Oxford Circus", "Tottenham Court Road", "Holborn", "Chancery Lane", "St. Paul's","Bank/Monument", "Liverpool Street"});
        Line district1 = new Line("District", new String[]{"Earl's Court","Gloucester Road", "South Kensington", "Sloane Square", "Victoria", "St. James's Park", "Westminster", "Embankment", "Temple", "Blackfriars", "Mansion House", "Cannon Street", "Bank/Monument", "Tower Hill"});
        Line district2 = new Line("District (Edgware Road Branch)", new String[]{"Earl's Court", "High Street Kensington", "Notting Hill Gate", "Bayswater", "Paddington", "Edgware Road"});
        Line piccadilly = new Line("Piccadilly", new String[]{"Earl's Court","Gloucester Road", "South Kensington", "Knightsbridge", "Hyde Park Corner", "Green Park", "Piccadilly Circus", "Leicester Square", "Covent Garden", "Holborn", "Russell Square", "King's Cross St. Pancras"});
        Line victoria = new Line("Victoria", new String[]{"King's Cross St. Pancras","Euston","Warren Street", "Oxford Circus", "Green Park", "Victoria", "Pimlico"});
        Line northern1 = new Line("Northern (Charing Cross Branch)", new String[]{"Euston","Warren Street", "Goodge Street", "Tottenham Court Road", "Leicester Square", "Charing Cross", "Embankment", "Waterloo", "Kennington"});
        Line northern2 = new Line("Northern (Bank Branch)", new String[]{"Euston", "King's Cross St. Pancras", "Angel", "Old Street", "Moorgate", "Bank/Monument", "London Bridge", "Borough", "Elephant & Castle", "Kennington"});
        Line watercity = new Line("Waterloo & City", new String[]{"Waterloo", "Bank/Monument"});
        
        TubeMap map = new TubeMap(Line.allLines(), Station.allStations());
       
       System.out.println("Enter source station:");
       Scanner scanner = new Scanner(System.in);
       String source = scanner.nextLine().trim();
       System.out.println("Enter destination station:");
       String destination = scanner.nextLine().trim();
       try {
        map.getPaths(source,destination);
       } catch(Exception e) {
           System.out.println("Error! One of the stations is not valid");
       }
       map.printPaths();       
}
}
