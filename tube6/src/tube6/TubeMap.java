/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tube6;
import java.util.ArrayList;


public class TubeMap {
    
    private ArrayList<Station> all_stations;
    private ArrayList<Line> all_lines;
    private ArrayList<ArrayList<String>> paths = new ArrayList<ArrayList<String>>();
    private int shortest_path = 999;
    
    public TubeMap(ArrayList<Line> lns, ArrayList<Station> stns) {
        this.all_stations = stns;
        this.all_lines = lns;
    }
    private void cropPaths(Station destination) {
        int shortestpath = 99999;
        for(int i=paths.size()-1; i>=0; i--) {
            if(!paths.get(i).get(paths.get(i).size()-1).equals(destination.toString())) {
                paths.remove(i);
            } else {
                if(paths.get(i).size() < shortestpath) {
                    shortestpath = paths.get(i).size();
                }
            }
        }
        for(int i=paths.size()-1; i>-1; i--) {
            if(paths.get(i).size() > shortestpath) {
                paths.remove(i);
            }
        }
    }
    public void getPaths(String source, String destination) {
        findPaths(Station.get(source), Station.get(destination), null);
        cropPaths(Station.get(destination)); 
    }
    public boolean findPaths(Station source, Station destination, ArrayList<String> thispath) {

        if(source.toString().equals(destination.toString())) {
            return true;
        }
        ArrayList<String> connected = source.getConnected();
        if(thispath==null) {
            for(int i=0; i<connected.size(); i++) {
                ArrayList<String> currentpath = new ArrayList<String>();
                currentpath.add(source.toString());
                currentpath.add(connected.get(i).toString());
                paths.add(currentpath);
                for(int j=0; j<paths.size(); j++) {
                    ArrayList<String> cur = paths.get(j);  
                } 
            }
            for(int i=0; i<paths.size(); i++) {
                ArrayList<String> currentpath = paths.get(i);
                Station st = Station.get(currentpath.get(currentpath.size()-1));
                findPaths(st, destination, paths.get(i));
            }
        } else {
            if(thispath.size() > shortest_path) {
                return false;
            }
            Station last_stn = Station.get(thispath.get(thispath.size()-1));
            ArrayList<String> cstring = last_stn.getConnected();
            ArrayList<Station> connected_stns = new ArrayList<Station>();
            
            for(int i=0; i<cstring.size(); i++) {
                connected_stns.add(Station.get(cstring.get(i)));
            }
            for(int i=0; i<connected_stns.size(); i++) {
                Station currentstation = connected_stns.get(i);
                boolean exists=false;
                for(int j=0; j<thispath.size(); j++) {
                    if(thispath.get(j).toString().equals(currentstation.toString())) {
                        exists=true;
                    }
                }
                if(!exists) { 
                    ArrayList<String> newpath = (ArrayList<String>)thispath.clone();
                    newpath.add(currentstation.toString());

                    paths.add(newpath);
                    if(currentstation.toString().equals(destination.toString())) {
                        if(newpath.size() < shortest_path) {
                           shortest_path = newpath.size();
                        }
                        return false;
                    }                    
                }
            }
        } 
        if(thispath==null) {
            System.out.println("Shortest path length: "+shortest_path);
        }
        return false;
    }
    public void printPaths() {
        StringBuilder a = new StringBuilder();
         for(int i=0; i<paths.size(); i++) {
            a.append(("Path "+i));
            a.append("\n");
            for(int j=0; j<paths.get(i).size(); j++) {
                a.append(paths.get(i).get(j));
                a.append("\n");
            }
            a.append("\n");
        }
         System.out.println(a.toString());
    }
    
}
