package tube6;
import java.util.ArrayList;

public class Station {
    
    private String name;
    private ArrayList<String> lines = new ArrayList<String>();
    private static ArrayList<Station> all_stations = new ArrayList<Station>();
   
    public Station(String name, String initial_line) {
        this.name = name;
        lines.add(initial_line);
        addStation();
    }
    public static Station get(String lname) {
        for(int i=0; i<all_stations.size(); i++) {
            if(all_stations.get(i).toString().equals(lname)) {
                return all_stations.get(i);
            }
        }
        return null;
    }   
    
    @Override 
    public String toString() {
        return this.name;
    }
    
    private void addStation() {
        all_stations.add(this);
    }
    public void addLine(String line) {
        lines.add(line);
    }
    
    public static ArrayList<Station> allStations() {
        return all_stations;
    }
    
    public ArrayList<String> getConnected() {
        String[] linestr = this.getLines();
        Line[] connectedlines = new Line[linestr.length];
        
        for(int i=0; i<linestr.length; i++) {
            connectedlines[i] = Line.get(linestr[i]);
        }
        ArrayList<String> connected = new ArrayList<String>();
        for(int i=0; i<connectedlines.length; i++) {
            Line a = connectedlines[i];
            String next = null;
            try {
                next = a.getNextOrPrevious(this.toString(),Line.NEXT);
            } catch(Exception e) {}
            String previous = null;
            try {
                previous =  a.getNextOrPrevious(this.toString(), Line.PREVIOUS);              
            } catch(Exception e) {}
            if(next!=null && !connected.contains(next)) {
                connected.add(next);
            }
            if(previous!=null && !connected.contains(previous)) {
                connected.add(previous);
            }
        }
        return connected;
    }
    
    public String[] getLines() { //return lines?? NO this is needed for a reason.
        String[] linestring = new String[lines.size()];
        for(int i=0; i<lines.size(); i++) {
            linestring[i]=lines.get(i);
        }
        return linestring;
    }
}