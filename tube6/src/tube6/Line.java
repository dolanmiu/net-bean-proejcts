
package tube6;

import java.util.ArrayList;

public class Line {
    
    private String name;
    private String[] stations;
    
    private static ArrayList<Line> all_lines = new ArrayList<Line>();
    public static final int NEXT = 99, PREVIOUS = 88; //alter?
    
    public Line(String name, String[] stations) {
        this.name = name;
        this.stations = stations;
        ArrayList<Station> all_stations = Station.allStations();
        
        for(int i=0; i<stations.length; i++) {
            boolean exists=false;
            String currentstation = stations[i];
            int j=0;
            for(j=0; j<all_stations.size(); j++) {
                if(currentstation.equals(all_stations.get(j).toString())) {
                    exists = true;
                    break;
                }
            }    
            if(exists) {
                all_stations.get(j).addLine(name);
            } else {
                Station station = new Station(stations[i], name);
            }
        }  
        add();
    }
    
    public static ArrayList<Line> allLines() {
        return all_lines;
    }
    
    public static Line get(String lname) {
        for(int i=0; i<all_lines.size(); i++) {
            Line current = all_lines.get(i);
            if(all_lines.get(i).toString().equals(lname)) {
                return all_lines.get(i);
            }
        }
        return null;
    }
   // northernline.getNextOrPrevious("Embankment",Line.NEXT);
        public String getNextOrPrevious(String stn, int np) throws IllegalArgumentException {
        if(np!=NEXT && np!= PREVIOUS) {
            throw new IllegalArgumentException("2nd Argument must equal Line.NEXT or Line.PREVIOUS");
        }
        for(int i=0; i<stations.length; i++) {
            if(stations[i].equals(stn)) {
                try {
                    if(np==NEXT) {
                        return stations[i+1];
                    } else if(np==PREVIOUS) {
                        return stations[i-1];
                    }
                } catch(Exception e) {
                    return null;
                }
            }
        }
        return null;
    }
    private void add() {
        all_lines.add(this);
    }
    @Override
    public String toString() {
        return name;
    }
}
